/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.api;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.prp.auth.api.pojo.ApiExceptionResponse;
import com.prp.auth.exception.PRPErrorCode;
import com.prp.auth.exception.PRPException;

@ControllerAdvice(basePackages = {
		"com.prp.auth.api"
})
public class GlobalExceptionController extends ResponseEntityExceptionHandler {

	@ExceptionHandler(PRPException.class)
	protected ResponseEntity<Object> handlePRPException(final PRPException ex,
			final WebRequest request) {
		HttpStatus httpStatus;
		switch (ex.getErrorCode()) {
			case AUTHENTICATION_BEAN_BASIC_CRIDENTIALS_NOT_GIVEN:
			case AUTHENTICATION_BEAN_NO_PERMISSIONS_GIVEN:
				httpStatus = HttpStatus.UNAUTHORIZED;
				break;
			case AUTHENTICATION_BEAN_EMAIL_MISSING:
			case AUTHENTICATION_BEAN_NO_CONFIRMATION_CODE_PROVIDED:
				httpStatus = HttpStatus.BAD_REQUEST;
				break;
			case AUTHENTICATION_DAL_USER_NOT_FOUND:
			case AUTHENTICATION_BEAN_NO_USER_FOR_CONF_CODE_FOUND:
			case AUTHENTICATION_DAL_USER_DOES_NOT_EXIST:
				httpStatus = HttpStatus.NO_CONTENT;
				break;
			case AUTHENTICATION_DAL_USER_ALREADY_REGISTERED:
				httpStatus = HttpStatus.CONFLICT;
				break;
			default:
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		ApiExceptionResponse exceptionResponse = ApiExceptionResponse.builder()
				.error(ex.getClass().getName()).message(ex.getMessage()).status(httpStatus.value())
				.path(((ServletWebRequest) request).getRequest().getRequestURI())
				.internalErrorCode(ex.getErrorCode()).build();
		return handleExceptionInternal(ex, exceptionResponse, new HttpHeaders(), httpStatus,
				request);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> defaultHandler(final Exception ex, final WebRequest request) {
		ApiExceptionResponse exceptionResponse = ApiExceptionResponse.builder()
				.error(ex.getClass().getName()).message(ex.getMessage())
				.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.path(((ServletWebRequest) request).getRequest().getRequestURI())
				.internalErrorCode(PRPErrorCode.UNKNOWN).build();
		return handleExceptionInternal(ex, exceptionResponse, new HttpHeaders(),
				HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

}
