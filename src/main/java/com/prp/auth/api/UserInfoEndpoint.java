/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.api;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.prp.auth.bean.UserManagementBean;
import com.prp.auth.pojo.User;
import com.prp.auth.exception.PRPException;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserInfoEndpoint {

	private UserManagementBean userManagementBean;

	@Autowired
	public UserInfoEndpoint(final UserManagementBean userManagementBean) {
		this.userManagementBean = userManagementBean;
	}

	@GetMapping("/user/me")
	@ResponseBody
	public Principal user(final Principal principal) throws PRPException {
		User user = userManagementBean.loadUser(principal.getName());
		@SuppressWarnings("unused")
		Principal retVal = new Principal() {

			@Override
			public String getName() {
				return user.getId().toString();
			}

			public String getPrimaryEmail() {
				return user.getPrimaryEmail();
			}
		};
		return retVal;
		// return principal;
	}

}
