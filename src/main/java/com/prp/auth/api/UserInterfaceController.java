/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.prp.auth.bean.RegistrationBean;
import com.prp.auth.pojo.User;
import com.prp.auth.exception.PRPException;
import com.prp.auth.exception.PRPErrorCode;

@Controller
public class UserInterfaceController {

    private static final Logger log = LoggerFactory.getLogger(UserInterfaceController.class);

    private static final String LOGIN_TEMPLATE = "login";
    private static final String SIGNUP_TEMPLATE = "signup";
    private static final String SIGNUP_FEEDBACK_TEMPLATE = "signup_feedback";
    private static final String CONFIRMATION_FEEDBACK_TEMPLATE = "confirmation_feedback";
    private static final String ERROR_ATTRIBUTE = "error";
    private static final String ERROR_CODE = "error_code";

    private RegistrationBean registrationBean;

    @Autowired
    public UserInterfaceController(final RegistrationBean registrationBean) {
        this.registrationBean = registrationBean;
    }

    @GetMapping("/login")
    public String login() {
        return LOGIN_TEMPLATE;
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signup() {
        log.debug("/signup (GET) called");
        return SIGNUP_TEMPLATE;
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signupExcecute(final Model model, @RequestParam(name = "email") final String email,
                                 @RequestParam(name = "password") final String password,
                                 @RequestParam(name = "password2") final String checkPassword) throws PRPException {

        log.debug("/signup (POST) called");

        if (password.isEmpty() || checkPassword.isEmpty()) {
            model.addAttribute(ERROR_ATTRIBUTE, "passwordEmpty");
            return SIGNUP_TEMPLATE;
        }
        if (!password.equals(checkPassword)) {
            model.addAttribute(ERROR_ATTRIBUTE, "passwordsNotEqual");
            return SIGNUP_TEMPLATE;
        }
        User registrationUser = new User();
        registrationUser.setPrimaryEmail(email);
        registrationUser.setPassword(password);
        try {
            // TODO Nullpointer Exception because E-Mail is currently not sent
            try {
                registrationBean.registerUser(registrationUser);
            } catch (NullPointerException i) {
                log.debug("NullPointer");
            }
        } catch (PRPException e) {
            log.debug("Got error from backend: " + e.getErrorCode().toString());
            switch (e.getErrorCode()) {
                case AUTHENTICATION_DAL_USER_ALREADY_REGISTERED:
                    model.addAttribute(ERROR_ATTRIBUTE, "emailAlreadyUsed");
                    model.addAttribute(ERROR_CODE, e.getErrorCode().getCode());
                    return SIGNUP_TEMPLATE;
                case AUTHENTICATION_BEAN_BASIC_CRIDENTIALS_NOT_GIVEN:
                    model.addAttribute(ERROR_ATTRIBUTE, "passwordEmpty");
                    model.addAttribute(ERROR_CODE, e.getErrorCode().getCode());
                    return SIGNUP_TEMPLATE;
                default:
                    model.addAttribute(ERROR_ATTRIBUTE, "undefined");
                    model.addAttribute(ERROR_CODE, e.getErrorCode().getCode());
                    break;
            }
        }
        return SIGNUP_FEEDBACK_TEMPLATE;
    }

    @RequestMapping(value = "/confirm/{code}", method = RequestMethod.GET)
    public String confirmAccount(final Model model, @PathVariable(name = "code") final String confCode) {
        try {
            registrationBean.confirmUser(confCode);
        } catch (PRPException e) {
            log.debug("Got error from backend: " + e.getErrorCode().toString());
            switch (e.getErrorCode()) {
                case AUTHENTICATION_DAL_USER_ALREADY_REGISTERED:
                    model.addAttribute(ERROR_ATTRIBUTE, "userAlreadyRegistered");
                    model.addAttribute(ERROR_CODE, e.getErrorCode().getCode());
                    return CONFIRMATION_FEEDBACK_TEMPLATE;
                case AUTHENTICATION_BEAN_NO_CONFIRMATION_CODE_PROVIDED:
                case AUTHENTICATION_DAL_NOT_ALL_ATTRIBUTES_GIVEN:
                case AUTHENTICATION_DAL_USER_NOT_FOUND:
                case AUTHENTICATION_DAL_CONFIRMATIONCODE_MISSING:
                case AUTHENTICATION_BEAN_NO_USER_FOR_CONF_CODE_FOUND:
                case AUTHENTICATION_DAL_USER_DOES_NOT_EXIST:
                    model.addAttribute(ERROR_ATTRIBUTE, "internalError");
                    model.addAttribute(ERROR_CODE, e.getErrorCode().getCode());
                    return CONFIRMATION_FEEDBACK_TEMPLATE;
                default:
                    break;

            }
        }
        return CONFIRMATION_FEEDBACK_TEMPLATE;
    }

    @GetMapping("/logout")
    public String logoutPage(final HttpServletRequest request, final HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

}
