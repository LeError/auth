/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.trace.http.HttpTraceRepository;
import org.springframework.boot.actuate.trace.http.InMemoryHttpTraceRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import com.prp.auth.security.PRPApprovalHandler;

@Configuration
@EnableAuthorizationServer
public class AuthServerConfig extends AuthorizationServerConfigurerAdapter {

	private BCryptPasswordEncoder passwordEncoder;
	private AuthenticationManager authManager;
	private TokenStore tokenStore;
	private JwtAccessTokenConverter tokenConverter;
	private PRPApprovalHandler approvalHandler;

	@Autowired
	public AuthServerConfig(final BCryptPasswordEncoder passwordEncoder, final AuthenticationManager authManager,
			final TokenStore tokenStore, final JwtAccessTokenConverter tokenConverter,
			final PRPApprovalHandler approvalHandler) {
		this.passwordEncoder = passwordEncoder;
		this.authManager = authManager;
		this.tokenStore = tokenStore;
		this.tokenConverter = tokenConverter;
		this.approvalHandler = approvalHandler;
	}

	@Override
	public void configure(final AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
		oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
	}

	@Override
	public void configure(final ClientDetailsServiceConfigurer clients) throws Exception {
		// @formatter:off
		clients.inMemory()
			.withClient("CordovaWeb")
				.secret(passwordEncoder.encode("secret"))
				.authorizedGrantTypes("password")
				.scopes("login")
				.autoApprove(true)
				.resourceIds("oauth2-resource")
				.redirectUris("http://localhost:8000/index.html", "http://192.168.2.102:8000/index.html")
		.and()
			.withClient("PRPBackend")
			.secret(passwordEncoder.encode("secret"))
			.authorizedGrantTypes("client_cridentials")
			.scopes("any")
			.autoApprove(true)
			.resourceIds("oauth2-resource")
			.redirectUris("http://localhost:8080/api/0.1/calendar/create")
		.and()
			.withClient("PRPResource")
			.secret(passwordEncoder.encode("secret"))
			.authorizedGrantTypes("client_cridentials")
			.scopes("any")
			.autoApprove(true)
			.resourceIds("oauth2-resource")
			.redirectUris("http://localhost:8080/api/0.1/calendar/create");
		// @formatter:on
	}

	@Override
	public void configure(final AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		// @formatter:off
		endpoints
			.tokenStore(tokenStore)
			.accessTokenConverter(tokenConverter)
			.authenticationManager(authManager)
			.userApprovalHandler(approvalHandler);
		// @formatter:on
	}

	@Bean
	@Order(2)
	public CommonsRequestLoggingFilter requestLogging() {
		CommonsRequestLoggingFilter filter = new CommonsRequestLoggingFilter();
		filter.setIncludeQueryString(true);
		filter.setIncludePayload(true);
		filter.setMaxPayloadLength(10000);
		filter.setIncludeHeaders(true);
		filter.setAfterMessagePrefix("REQUEST DATA : ");
		return filter;
	}

	@Bean
	public HttpTraceRepository httpTraceRepositoryBean() {
		return new InMemoryHttpTraceRepository();
	}

}
