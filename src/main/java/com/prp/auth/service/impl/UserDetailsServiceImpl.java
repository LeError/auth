/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.prp.auth.bean.UserManagementBean;
import com.prp.auth.pojo.User;
import com.prp.auth.service.model.UserDetailsImpl;
import com.prp.auth.exception.PRPException;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

	private static final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

	private UserManagementBean userManagementBean;

	@Autowired
	public UserDetailsServiceImpl(final UserManagementBean userManagementBean) {
		this.userManagementBean = userManagementBean;
	}

	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		User user;
		try {
			user = userManagementBean.loadUser(username);
		} catch (PRPException e) {
			log.error("User could not be loaded.", e);
			throw new UsernameNotFoundException(e.getMessage(), e);
		}
		UserDetailsImpl retVal = new UserDetailsImpl(user);
		return retVal;
	}

}
