/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.security;

import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

import com.prp.auth.security.converter.PRPAccessTokenConverter;
import com.prp.auth.security.properties.SecurityProperties;

@Configuration
@Order(1)
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableConfigurationProperties(SecurityProperties.class)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private UserDetailsService userDetailsService;
    private SecurityProperties props;

    @Autowired
    public SecurityConfig(final UserDetailsService userDetailsService, final SecurityProperties props) {
        this.userDetailsService = userDetailsService;
        this.props = props;
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        // @formatter:off
		http.requestMatchers().and()
		.authorizeRequests()
		.antMatchers(HttpMethod.GET, "/swagger-ui/", "/v3/api-docs/**").permitAll()
		.antMatchers("/actuator/**", "/favicon.ico").permitAll()
		.antMatchers(HttpMethod.GET, "/login", "/confirm/**", "/signup",
				"/oauth/authorize", "/css/**", "/img/**").permitAll()
		.antMatchers(HttpMethod.OPTIONS, "/oauth/token").permitAll()
		.antMatchers(HttpMethod.POST, "/oauth/token", "/signup").permitAll()
		.and().formLogin().loginPage("/login").permitAll()
		.and().authorizeRequests().anyRequest().authenticated()
		.and().headers().frameOptions().disable()
		.and().cors().configurationSource(new CorsConfigurationSource() {

					@Override
					public CorsConfiguration getCorsConfiguration(final HttpServletRequest request) {
						CorsConfiguration conf = new CorsConfiguration();
						List<String> allowedOrigins = new ArrayList<>();
						allowedOrigins.add("http://localhost:8000");
						allowedOrigins.add("http://localhost:8080");
						conf.setAllowedOrigins(allowedOrigins);
						List<String> allowedMethods = new ArrayList<>();
						allowedMethods.add("POST");
						allowedMethods.add("GET");
						conf.setAllowedMethods(allowedMethods);
						return conf;
					}
				})
		.and().csrf().disable();
		// @formatter:on
        // TODO enable frame options
        /*
         * new XFrameOptionsHeaderWriter(new StaticAllowFromStrategy(new URI("http://localhost:8000")))
         */
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(tokenConverter());
    }

    @Primary
    @Bean
    public RemoteTokenServices tokenService() {
        RemoteTokenServices tokenService = new RemoteTokenServices();
        tokenService.setCheckTokenEndpointUrl("http://localhost:8081/oauth/check_token");
        tokenService.setClientId("PRPBackend");
        tokenService.setClientSecret("secret");
        tokenService.setAccessTokenConverter(tokenConverter());
        return tokenService;
    }

    @Bean
    public JwtAccessTokenConverter tokenConverter() {
        JwtAccessTokenConverter conv = new JwtAccessTokenConverter();
        conv.setKeyPair(keyPair());
        conv.setAccessTokenConverter(new PRPAccessTokenConverter());
        return conv;
    }

    @Bean
    public KeyPair keyPair() {
        KeyStoreKeyFactory factory = new KeyStoreKeyFactory(props.getJwt().getKeyStore(),
                props.getJwt().getKeyStorePassword().toCharArray());
        return factory.getKeyPair(props.getJwt().getKeyPairAlias(), props.getJwt().getKeyPairPassword().toCharArray());
    }
}
