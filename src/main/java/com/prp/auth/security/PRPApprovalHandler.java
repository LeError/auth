/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.approval.DefaultUserApprovalHandler;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.stereotype.Component;

@Component
public class PRPApprovalHandler extends DefaultUserApprovalHandler implements UserApprovalHandler {

	@Override
	public boolean isApproved(final AuthorizationRequest authorizationRequest,
			final Authentication userAuthentication) {
		return true;
	}

	@Override
	public AuthorizationRequest checkForPreApproval(final AuthorizationRequest authorizationRequest,
			final Authentication userAuthentication) {
		authorizationRequest.setApproved(true);
		return authorizationRequest;
	}

	@Override
	public AuthorizationRequest updateAfterApproval(final AuthorizationRequest authorizationRequest,
			final Authentication userAuthentication) {
		authorizationRequest.setApproved(true);
		return authorizationRequest;
	}

}
