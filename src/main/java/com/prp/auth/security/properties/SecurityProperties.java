/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.security.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;

@ConfigurationProperties("security")
public class SecurityProperties {
	private JwtProperties jwt;

	public JwtProperties getJwt() {
		return jwt;
	}

	public void setJwt(final JwtProperties jwt) {
		this.jwt = jwt;
	}

	public static class JwtProperties {

		private Resource keyStore;
		private String keyStorePassword;
		private String keyPairAlias;
		private String keyPairPassword;

		public Resource getKeyStore() {
			return keyStore;
		}

		public void setKeyStore(final Resource keyStore) {
			this.keyStore = keyStore;
		}

		public String getKeyStorePassword() {
			return keyStorePassword;
		}

		public void setKeyStorePassword(final String keyStorePassword) {
			this.keyStorePassword = keyStorePassword;
		}

		public String getKeyPairAlias() {
			return keyPairAlias;
		}

		public void setKeyPairAlias(final String keyPairAlias) {
			this.keyPairAlias = keyPairAlias;
		}

		public String getKeyPairPassword() {
			return keyPairPassword;
		}

		public void setKeyPairPassword(final String keyPairPassword) {
			this.keyPairPassword = keyPairPassword;
		}
	}
}
