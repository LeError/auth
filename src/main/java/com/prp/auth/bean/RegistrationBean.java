/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.bean;

import com.prp.auth.pojo.User;
import com.prp.auth.security.enums.UserActivationState;
import com.prp.auth.exception.PRPException;

/**
 * Bean responsible for registration process.
 * <p>
 * Important tasks are the registration, the storing of a new user to the system and the user confirmation via email.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see User
 */
public interface RegistrationBean {

	/**
	 * Stores an {@link User} to the system.
	 * <p>
	 * Also, this methods creates the confirmation code and sends the confirmation email.
	 *
	 * @param user - The {@link User} to store. Required fields are the primary email address, password and the permissions.
	 * @return {@link User} - The registered user.
	 * @throws LiaException
	 */
	User registerUser(User user) throws PRPException;

	/**
	 * This method confirms, that the given confirmation code exists and sets the underlying {@link User} to {@link UserActivationState#ACTIVE}.
	 *
	 * @param confirmationCode - The {@link String} the user entered for confirmation.
	 * @return {@link User} the confirmed user.
	 * @throws LiaException
	 */
	User confirmUser(String confirmationCode) throws PRPException;

}
