/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.bean.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.prp.auth.bean.RegistrationBean;
import com.prp.auth.dal.UserDal;
import com.prp.auth.dal.model.UserDB;
import com.prp.auth.pojo.User;
import com.prp.auth.service.EmailService;
import com.prp.auth.util.PasswordGenerator;
import com.prp.auth.util.UserConverter;
import com.prp.auth.exception.PRPErrorCode;
import com.prp.auth.security.enums.UserActivationState;
import com.prp.auth.exception.PRPException;

/**
 * @since 0.1
 * @see RegistrationBean
 * @author Eric Fischer
 *
 */
@Component
public class RegistrationBeanImpl implements RegistrationBean {

	private static final Logger log = LoggerFactory.getLogger(RegistrationBeanImpl.class);

	private UserDal userDal;
	private EmailService emailService;
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	public RegistrationBeanImpl(final UserDal userDal, final EmailService backendService,
			final BCryptPasswordEncoder passwordEncoder) {
		this.userDal = userDal;
		this.emailService = backendService;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public User registerUser(final User user) throws PRPException {
		if (user.getPrimaryEmail() == null || user.getPrimaryEmail().isEmpty() || user.getPassword() == null
				|| user.getPassword().isEmpty()) {
			String message = "Cridentials for basic authentification not given.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_BEAN_BASIC_CRIDENTIALS_NOT_GIVEN);
		}

		if (user.getPermissions() == null || user.getPermissions().isEmpty()) {
			String message = "No permissions for user given.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_BEAN_NO_PERMISSIONS_GIVEN);
		}

		// password encryption
		user.setPassword(passwordEncoder.encode(user.getPassword()));

		UserDB stored = userDal.storeUser(UserConverter.convertUser(user),
				UserConverter.convertPermissions(user.getPermissions()));

		User storedUser = UserConverter.convertUser(stored);

		String confKey = generateEmailConfirmationKey();
		userDal.storeConfirmationCode(stored, confKey);

		emailService.sendMail(stored.getEmail(), confKey);

		return storedUser;
	}

	@Override
	public User confirmUser(final String confirmationCode) throws PRPException {
		if (confirmationCode == null || confirmationCode.isEmpty()) {
			String message = "No confirmation code provided.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_BEAN_NO_CONFIRMATION_CODE_PROVIDED);
		}

		UserDB user = userDal.findUserForConfirmationCode(confirmationCode);

		if (user == null) {
			String message = "No user found for the given code.";
			log.warn(message);
			throw new PRPException(message, PRPErrorCode.AUTHENTICATION_BEAN_NO_USER_FOR_CONF_CODE_FOUND);
		}

		user.setActive(UserActivationState.ACTIVE);
		userDal.updateUser(user);

		userDal.deleteConfirmationCode(confirmationCode);

		UserDB loadedUser = userDal.loadUser(user.getEmail());

		return UserConverter.convertUser(loadedUser);
	}

	private String generateEmailConfirmationKey() throws PRPException {
		PasswordGenerator passwordGenerator = new PasswordGenerator.PasswordGeneratorBuilder().useDigits(true)
				.useLower(true).useUpper(true).build();
		String password = passwordGenerator.generate(8);
		if (userDal.findUserForConfirmationCode(password) != null) {
			return generateEmailConfirmationKey();
		}
		log.debug("Generated confirmation key: " + password);
		return password;
	}

}
