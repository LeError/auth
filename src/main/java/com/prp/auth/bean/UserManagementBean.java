/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.bean;

import com.prp.auth.pojo.User;
import com.prp.auth.exception.PRPException;

/**
 * The bean responsible for managing registered users. Important tasks are the loading of general user information, the updating of the
 * information and deleting users.
 * <p>
 * The most important method is {@link #deleteUserData(User)}. It deletes all data linked with an {@link User} within the system. This is
 * necessary because of EU's GDPR.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see User
 *
 */
public interface UserManagementBean {

	/**
	 * Searches for an user according to the given email ({@link String}).
	 *
	 * @param primaryEmail - The {@link String} to search for.
	 * @return {@link User} - The user found in the system or <code>null</code> if nothing was found.
	 * @throws LiaException
	 * @since 0.1
	 */
	User loadUser(String primaryEmail) throws PRPException;

	/**
	 * Updates user information. It isn't required to tell this method, which information has to be updated. Just hand the {@link User} you wish
	 * to update and the system does.
	 * <p>
	 * Remember to hand the ID of the user, this is the only indicator, if an database entry belongs to the user. If no user with the given ID
	 * could be found, a {@link LiaException} is thrown.
	 *
	 * @param user - The {@link User} to update. Remeber to hand over the ID of the user.
	 * @return {@link User} - The updated user.
	 * @throws PRPException
	 * @since 0.1
	 */
	User updateUser(User user) throws PRPException;

	/**
	 * This method deletes all user data related to an user account within the system. So it is not just deleting the {@link User} information,
	 * it is deleting all accounts, messages, events, tasks, contacts and whatever the system stored related to the user.
	 * <p>
	 * This way of deleting information is necessary because of EU' GDPR. This functionality is directly implementing the rule of deleting all
	 * data related to an EU citizen, if he or she wishes.
	 * <p>
	 * This action isn't revertable. General and anonimized information will be kept for analytical purposes.
	 * <p>
	 * Technically, every microservice has to implement a special API in order to be accassable for this bean method.
	 *
	 * @param user - The {@link User} to delete all data for. Remember to hand the ID as the only safe identifier with the user.
	 * @throws PRPException
	 * @since 0.1
	 */
	void deleteUserData(User user) throws PRPException;

}
