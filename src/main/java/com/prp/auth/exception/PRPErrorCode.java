/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.exception;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * Error code registry for the PRP Auth Service. In this enum, every case used in this
 * Service is documented.
 * <p>
 * The purpose of these codes is to hand the reason for an {@link PRPException}
 * to the client or the calling service. This way, it is possible to create
 * understandable error messages.
 * <p>
 * <strong>Important to know:</strong> Not every error case explained here has
 * to be handed to the client as it is. Most of them, specially DAL errors,
 * indicate internal bugs. In this case, some general information should be
 * displayed to the user with the specific error code. This way, it is possible
 * to track bugs.
 *
 * @see PRPException
 * @author Eric Fischer
 * @since 0.1
 *
 */
public enum PRPErrorCode {

	/******************
	 * General errors *
	 *****************/

	/**
	 * This should be the default case in {@link LiaException}. If this error is
	 * handed to the client, something went completely wrong and should be treated
	 * as a bug.
	 *
	 * @since 0.1
	 */
	UNKNOWN("0"),

	/***************************
	 * Authentication errors *
	 **************************/

	/**
	 * Thrown, if username or password are not handed to the bean. In this case, the
	 * registration can't be completed.
	 *
	 * @since 0.1
	 */
	AUTHENTICATION_BEAN_BASIC_CRIDENTIALS_NOT_GIVEN("AUTH_B_BCNG"),
	/**
	 * Thrown if a user can't be registered because no permissions are handed to the
	 * user.
	 *
	 * @since 0.1
	 */
	AUTHENTICATION_BEAN_NO_PERMISSIONS_GIVEN("AUTH_B_NPG"),
	/**
	 * Thrown, if the required email isn't provided to search for an user.
	 *
	 * @since 0.1
	 */
	AUTHENTICATION_BEAN_EMAIL_MISSING("AUTH_B_EM"),
	/**
	 * Thrown if no confirmation code for user email confirmation provided.
	 * <p>
	 * As it is the only purpose for this method to check the confirmation code,
	 * this definitely indicates a bug.
	 *
	 * @since 0.1
	 */
	AUTHENTICATION_BEAN_NO_CONFIRMATION_CODE_PROVIDED("AUTH_B_NCCP"),
	/**
	 * Thrown if the user to store to the database has an email which is already
	 * stored. In this case, storing the user has to be quit.
	 *
	 * @since 0.1
	 */
	AUTHENTICATION_DAL_USER_ALREADY_REGISTERED("AUTH_D_UAR"),
	/**
	 * Thrown if not all needed information is handed to the database access layer.
	 * <p>
	 * This is an internal error. If thrown, the user object given was not filled
	 * correctly. If this error occurs, there's definitely a bug in the application.
	 *
	 * @since 0.1
	 */
	AUTHENTICATION_DAL_NOT_ALL_ATTRIBUTES_GIVEN("AUTH_D_NAAG"),
	/**
	 * No user could be found for the given email.
	 * <p>
	 * This means, that the user which had been searched for isn't registered or the
	 * email is free to use for registration.
	 *
	 * @since 0.1
	 */
	AUTHENTICATION_DAL_USER_NOT_FOUND("AUTH_D_UNF"),
	/**
	 * No confirmation code entry for the given string found. In this case,
	 * confirmation cannot be completed.
	 * <p>
	 * This could mean, that the code was already used.
	 *
	 * @since 0.1
	 */
	AUTHENTICATION_BEAN_NO_USER_FOR_CONF_CODE_FOUND("AUTH_B_NUFCCF"),
	/**
	 * No user could be found for the given id.
	 * <p>
	 * In case this error occurred while searching the user for a confirmation code,
	 * the user could be deleted or something went wrong while storing the
	 * confirmation code.
	 *
	 * @since 0.1
	 */
	AUTHENTICATION_DAL_USER_DOES_NOT_EXIST("AUTH_D_UIDNE"),
	/**
	 * Thrown if no confirmation code is handed to store or search in the database.
	 * <p>
	 * This one definitely indicates an bug in the system.
	 *
	 * @since 0.1
	 */
	AUTHENTICATION_DAL_CONFIRMATIONCODE_MISSING("AUTH_D_CM"),
	/**
	 * The handed confirmation code hasn't the required minimal length.
	 * <p>
	 * Every code should have a minimal length of 8. If this one is fired, there's a
	 * bug within the application.
	 *
	 * @since 0.1
	 */
	AUTHENTICATION_DAL_CONFIRMATIONCODE_TOO_SHORT("AUTH_D_CTS"),
	/**
	 * Thrown if no user is handed to the method or the required user id is missing.
	 * <p>
	 * This one definitely indicates an bug in the system.
	 *
	 *
	 * @since 0.1
	 */
	AUTHENTICATION_DAL_CONFIRMATIONCODE_USER_PARAMETER_MISSING("AUTH_D_CUSPM"),
	/**
	 * The confirmation code that should be stored is already in DB.
	 * <p>
	 * Definitely indicates a bug.
	 *
	 * @since 0.1
	 */
	AUTHENTICATION_DAL_CONFIRMATIONCODE_ALREADY_STORED("AUTH_D_CAS");

	@Getter
	private final String code;

	PRPErrorCode(final String code) {
		this.code = code;
	}

}
