/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.pojo;

import java.util.HashSet;
import java.util.Set;


import com.prp.auth.security.enums.UserActivationState;
import com.prp.auth.security.enums.UserPermissions;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Model for transporting information about users.
 * <p>
 * Provided information are email, which is treated as the username in this application, password (not transported to clients and API
 * accessors), Accounts belonging to the user and it's permissions.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see UserActivationState
 * @see UserPermissions
 * @see Account
 */
@EqualsAndHashCode
public class User {

	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	private String primaryEmail;

	@Getter
	@Setter
	private String password;

	@Getter
	@Setter
	private UserActivationState activationState;

	@Getter
	@Setter
	private Set<UserPermissions> permissions = new HashSet<>();

	/**
	 * When creating a user, standard permissions ({@link UserPermissions#USER}) and {@link UserActivationState#WAITING_FOR_ACTIVATION} is set.
	 *
	 * @since 0.1
	 */
	public User() {
		permissions.add(UserPermissions.USER);
		activationState = UserActivationState.WAITING_FOR_ACTIVATION;
	}

}
