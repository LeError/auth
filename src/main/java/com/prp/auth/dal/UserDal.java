/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.auth.dal;

import java.util.Set;

import com.prp.auth.dal.model.ConfirmationCodeDB;
import com.prp.auth.dal.model.UserDB;
import com.prp.auth.dal.model.UserPermissionsDB;
import com.prp.auth.exception.PRPException;

/**
 * Data access layer for accessing user data.
 * <p>
 * This DAL is used for all user operations including storing and reading confirmation codes.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see UserDB
 * @see ConfirmationCodeDB
 * @see UserPermissionsDB
 */
public interface UserDal {

	/**
	 * Loading the user for the given primary email address.
	 *
	 * @param email - The {@link String} to search for.
	 * @return {@link User} - The user found or <code>null</code>, if no user is registered with this email.
	 * @throws PRPException
	 * @since 0.1
	 */
	UserDB loadUser(String email) throws PRPException;

	/**
	 * Stores an {@link User} to the database. This is like the the INSERT statement of SQL. If there's already an user registered with the
	 * given primary email, the storing will be interrupted.
	 * <p>
	 * This method is divided from {@link #updateUser(UserDB)} in order to prevent unintentional updates.
	 *
	 * @param user        - The {@link UserDB} to store.
	 * @param permissions - The {@link UserPermissionsDB} to store related to the user.
	 * @return {@link UserDB} - The user stored.
	 * @throws PRPException
	 * @since 0.1
	 */
	UserDB storeUser(UserDB user, Set<UserPermissionsDB> permissions) throws PRPException;

	/**
	 * Updates {@link UserDB} information.
	 * <p>
	 * This method is divided from {@link #storeUser(UserDB, Set)} in order to prevent unintentional updates.
	 *
	 * @param user - The {@link UserDB} to update. Remember to hand the ID to this method, otherwise no update is possible.
	 * @return {@link UserDB} - The updated user.
	 * @throws PRPException
	 * @since 0.1
	 */
	UserDB updateUser(UserDB user) throws PRPException;

	/**
	 * Store a {@link ConfirmationCodeDB} related to an freshly registered user.
	 *
	 * @param user             - The {@link UserDB} the confirmation code should be stored for.
	 * @param confirmationCode - The {@link String} code to store.
	 * @throws PRPException
	 * @since 0.1
	 */
	void storeConfirmationCode(UserDB user, String confirmationCode) throws PRPException;

	/**
	 * Loads a confirmation code for the given user.
	 *
	 * @param user - The {@link UserDB} to search for.
	 * @return {@link String} - The confirmation code or <code>null</code> if nothing found.
	 * @throws PRPException
	 * @since 0.1
	 */
	String loadConfirmationCode(UserDB user) throws PRPException;

	/**
	 * Loads the user related to a confirmation code.
	 *
	 * @param confirmationCode - The {@link String} to search for.
	 * @return {@link UserDB} - The user found for the given code or <code>null</code> if nothing found.
	 * @throws PRPException
	 * @since 0.1
	 */
	UserDB findUserForConfirmationCode(String confirmationCode) throws PRPException;

	/**
	 * Deletes a confirmation code. This is required, if the user requests another code or already confirmed his/her account.
	 *
	 * @param confirmationCode - The {@link String} to search for.
	 * @throws PRPException
	 * @since 0.1
	 */
	void deleteConfirmationCode(String confirmationCode) throws PRPException;

}
